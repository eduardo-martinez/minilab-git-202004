
GitLab Repo emty.md

Crear rama master y editar
Crear rama feat1 y editar
Crear rama feat2 y editar

Editar feat1 y guardar 3 commits
Editar feat2 y guardar 2 commits

git remote add origin https://gitlab.com/ejmv.rosario/t0.git

git add/commit/push [remote]
git branch [--all]

varios autores modificando en diferentes ramas
git fetch : recupera novedades al repo local

git merge (con conflictos)
edit
git add (cierra los conflictos y almacena el merge)

git checkout HEXA (detached head)
git checkout -b new-branch (en el commit elegido)


git tag tag-name
git tag tag-name-a-commit 763fc23
git tag -a tag-name-annotated 763fc23 "Release especial de cuarentena"


Mala idea!!
git pull origin master (fetch/merge desde origin/master)

Conveniente:
git fetch origin master (fetch/no merge desde origin/master)
luego hacer merge conveniente

Otra:
git pull --rebase (fetch/rebase sin perder mis cambios y commits)

Cosas extrañas:
merge interactivo: git rebase -i [commit]
pick/squash + new message

 (https://www.youtube.com/watch?v=V5KrD7CmO4o)
 (https://www.youtube.com/watch?v=tukOm3Afd8s)