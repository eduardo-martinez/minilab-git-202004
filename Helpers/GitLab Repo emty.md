
# GitLab initial empty state

https://gitlab.com/ejmv.rosario/t0


## The repository for this project is empty

You can get started by cloning the repository or start adding files to it with one of the following options.

### Command line instructions

You can also upload existing files from your computer using the instructions below.


#### Git global setup

git config --global user.name "Eduardo Martinez"
git config --global user.email "ejmv.rosario@gmail.com"

#### Create a new repository

git clone https://gitlab.com/ejmv.rosario/t0.git
cd t0
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

#### Push an existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/ejmv.rosario/t0.git
git add .
git commit -m "Initial commit"
git push -u origin master

#### Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/ejmv.rosario/t0.git
git push -u origin --all
git push -u origin --tags