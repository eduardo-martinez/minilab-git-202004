# MiniLab sobre GIT

Conceptos:

- Herramientas:

  - Git:

    - URL: <http://git-scm.com/>
    - Provee:
      - CLI: git bash
      - GUI: git-gui, gitk
      - Instalar, identificar "username" y "email"

  - Code (MSVSCode)
  - Dudas frecuentes:
    - En qué rama estoy? git branch
      - crear: git branch nueva-rama
      - listar: git branch
      - cambiar: git checkout nueva-rama
      - crear, y cambiar: git checkout -b nueva-rama-2

- Repositorios

  - Local, Stage area, Remoto/s
  - Bare, Non-bare, Local
  - Files, Commits, Branches
  - Clone, Push, Fetch, Pull

  - Branching:

    - From, To: git branch new-branch [fron-here-or-not]
    - Ramas locales y remotas

  - Commits, Tags, Head, references
    - Qué es un commit
      - resumen, línea, explicaciones (típico: NRO-ticket, [ticket], #ticket, y otras convenciones)
    - Qué es un Tag
    - Cómo se relacionan los commits?
    - Qué es un branch
    - Cómo se relacionan los commits de un mismo branch?
    - Cómo se relacionan los commits entre branches?
    - Cómo se relacionan los branches (respondido :) )?
    -


    - Committing:
      - Files: tracked/untracked, staged/unstaged, committed/uncommited
      - Merge vs Rebase (FastForward, ReWriteHistory)
        - git checkout master && git merge --squash feature && git commit -m "merged feature into master"
        - git checkout feature && git rebase master
      - Merge into: rama destino
      - Merge from: rama origen

    - Checkout
      - Cambios en carpetas, repo local (.git/)
      - Checkout remote branch: "You are in a detached head!!?
        - Relación entre commits, branches y heads

    - Cuándo está qué cosa, y dónde
      - git clone -bare
      - git clone
      - git add: files/filders/current
      - git commit
      - git fetch
      - git pull
      - git push
      - git branch
      - git add . && git commit -m "message"


    - Merge
      - merge
      - rebase
      - conflictos (merge, edit, commit)
        - git status
        - git log
        - git log -p
        - git gui + gitk : ver diferencia y estado de ramas y commits
        - code : compare, solve, save, commit

    - Pull requests (https://www.atlassian.com/git/tutorials/making-a-pull-request)
      - From, To


    - Workflows:
      - Git CHAOS: Cada uno como le plazca
      - GitFlow y vecinos:
        - 2010: https://nvie.com/posts/a-successful-git-branching-model/
        - Adoptado por Neoris en https://globalcampus.neoris.net/course/view.php?id=289:
          - Branching and versioning: https://globalcampus.neoris.net/mod/resource/view.php?id=1766
          - Interesante: Ver Semantic Versioning (major.minor.patch.hotfix)
        - master (prod)
        - hotfixes (master -> hotfix/hf-XXXX -> master y develop)
        - develop (master -> dev : integración de features)
        - features (feature/ft-XXXX -> develop)
        - releases (develop -> release/rls-XXX : preparación pre prod )

    - Algún truco
      - git ammend -m "msg" (y variantes: git gui es un amigo)
      - git rename (o renombrar/save as)
      - git add XX
        - git reset HEAD XX
        - git checkout -- XX
        - git gui: amend
        - git log --oneline (bueno para listar novedades: CHANGELOG)
      - git commit --stash
      - worktree (mismo repositorio local, varias ramas)
      - hooks (pre-push, pre-commit) (https://sigmoidal.io/automatic-code-quality-checks-with-git-hooks/****)
      - git-bisect
****