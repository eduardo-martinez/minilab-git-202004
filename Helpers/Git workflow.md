# Workflow

## Estructura de ramas

|--|
| Branch |
||
|hotfix|

master

develop

feature

| Branch      | ... | ... | .   | .   | .   | .   | .   | .   |
| ----------- | --- | --- | --- | --- | --- | --- | --- | --- |
| hotfix ...  |     |     |     |     |     |     |     |     |
| master      | m1    |     |     |     |     |     |     |     |
| release ... |     |     |     |     |     |     |     |     |
| develop     |     |     |     |     |     |     |     |     |
| bugfix ...  |     |     |     |     |     |     |     |     |
| feature ... |     |     |     |     |     |     |     |     |
